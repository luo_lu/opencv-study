import numpy as np

a = np.array([1,2,3],np.complex)
print(a)

b = np.zeros((2,3,3),dtype=np.uint8,order="F")
print(b)
print(b.ndim)
#类似结构体，利用结构体，可构造结构体数组
struct_name_age = np.dtype([("name","uint8"),("age","uint8")])
c = np.array([(1,12),(1,13)],dtype=struct_name_age)
print(c)
print(c.shape)
print(c["name"])
print(c["age"])
#自定义结构体，迭代修改值失败
'''
for x in np.nditer(c,op_flags=["readwrite"]):
    print(x.ndim)
    print(x.shape)
    x[...] = 2*x'''
#手动生成一组序列数据
d = np.linspace(1,10,10)
print(d)
d=np.reshape(d,(2,5))
print(d)
print(d.T)
for y in np.nditer(d,op_flags=["readwrite"]):
    print(y.ndim)
    y[...] = np.sin(y)
print d

e=np.arange(10)
print(e)
print(e[2:4])#切片，左闭，右开
